#!/usr/bin/env python3


import requests, dotenv
from time import sleep
from faker import Faker
from requests import session
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.service import Service


WPSCAN_API_KEY = ''
AVERAGE_PAGE_LOAD_TIME = 5  # in seconds.
env = dotenv.dotenv_values()
service = Service(executable_path="./geckodriver")
base_url = env["MAIL_API_URL"]; api_key = env["MAIL_API_KEY"]
options = webdriver.FirefoxOptions()
fake = Faker(); client = session()
headers = {
    'User-Agent': "Nick's Client X",
    'Mailsac-Key': api_key
}; client.headers.update(headers)
fake_email = fake.email().split('@')[0] + '@mailsac.com'
fake_name = fake.name(); fake_pass = fake.password()

def check_if_exists(email):
    if '@' not in email:
        email += '@mailsac.com'
    return client.get(base_url + f'/addresses/{email}/availability').json()['owned']

while check_if_exists(fake_email):
    fake_email = fake.email().split('@')[0] + '@mailsac.com'
    check_if_exists(fake_email)

def main():
    resp = client.post(base_url + f'/addresses/{fake_email}').json()
    driver = webdriver.Firefox(service=service, options=options)

    driver.get('https://wpscan.com/register/')
    sleep(AVERAGE_PAGE_LOAD_TIME)

    driver.find_element(By.ID, 'user.name').send_keys(fake_name)
    driver.find_element(By.ID, 'user.email').send_keys(fake_email)
    driver.find_element(By.ID, 'user.password').send_keys(fake_pass)
    driver.find_element(By.ID, 'user.password_confirmation').send_keys(fake_pass)
    driver.find_element(By.ID, 'user.terms_accepted').send_keys(' ')
    driver.find_element(By.XPATH, '//*[@id="wps-sign-up-form"]/div/div/div/form/div[4]/div/div/div[3]/button').send_keys(' ')
    sleep(AVERAGE_PAGE_LOAD_TIME)

    resp = client.get(base_url + f'/addresses/{fake_email}/messages').json()
    confirm_url = resp[0]['links'][0]
    driver.get(confirm_url)
    sleep(AVERAGE_PAGE_LOAD_TIME)

    driver.find_element(By.NAME, 'email').send_keys(fake_email)
    driver.find_element(By.NAME, 'password').send_keys(fake_pass)
    driver.find_element(By.XPATH, '/html/body/div[1]/main/div[2]/div/div/div/div/form/div/div[5]/div/button').send_keys(' ')

    sleep(AVERAGE_PAGE_LOAD_TIME + 3.33)  # Additional 3.33 because, it sometimes takes more time (which breaks the script; even if you've got your API keys through browser.)
    # Finally, the API key!!
    WPSCAN_API_KEY = driver.find_element(By.XPATH, '/html/body/div[1]/main/div[2]/div/div/div[1]/div[2]/div[1]/div[1]/div[1]/p').text
    print("WPSCAN-API-KEY:", WPSCAN_API_KEY)

    resp = client.delete(base_url+f'/addresses/{fake_email}').json()

    driver.quit()


if __name__ == '__main__':
    main()
