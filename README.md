# WpXcan

WpScan with no 25/day API limitation. - just kidding ~ but yeah, a workaround :p

You'll have to rename/move `.env.example` to `.env` and use one API key afer signing up on mailsac.com

You will also need to download geckodriver from https://github.com/mozilla/geckodriver/releases 
Decompress and save, the excutable within this project's path.

Finally, you can now run `./main.py` or `python3 ./main.py` and obtain one free WP-Scan API Token.
